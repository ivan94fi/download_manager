import os
import pickle
import hashlib
from settings import app_config
import threading

# This was also meant to be a singleton, but again many suggested using a
# variable declared at module level, to be exported where needed.


class ConfigManager(object):
    """ This class manages the download configurations stored on disk.

        This class is responsible for maintaining a pickle file updated
        with the changes in running downloads. The pickle contains a
        dictionary, whose keys are the downlaod urls hashes.
    """

    def __init__(self, config_path):
        self._dirty = False
        self.config_path = config_path
        self._timer = threading.Timer(30, self._save_config_file)
        self._timer.start()
        if not os.path.exists(self.config_path):
            os.mknod(self.config_path)

        if os.path.getsize(self.config_path) == 0:
            with open(self.config_path, 'wb') as config_file:
                pickle.dump({}, config_file)
        self.config_dict = self._read_config_file()

    def _read_config_file(self):
        if self._dirty is True:
            self._save_config_file(self.config_dict)
        with open(self.config_path, 'rb') as config_file:
            config_dict = pickle.load(config_file)
        return config_dict

    def _save_config_file(self):
        if self._dirty:
            print("Saving configs to disk..")
            with open(self.config_path, 'wb') as config_file:
                pickle.dump(self.config_dict, config_file)
            self._dirty = False

    def store_config(self, download_config):
        url_hash = hash_string(download_config['url'])
        self.config_dict[url_hash] = download_config._dict
        self._dirty = True

    def remove_config(self, url):
        url_hash = hash_string(url)
        del self.config_dict[url_hash]
        self._dirty = True

    def read_config(self, url):
        url_hash = hash_string(url)
        return self.config_dict.setdefault(url_hash, None)

    def get_config_dict(self):
        return self.config_dict


config_manager = ConfigManager(
    app_config.get("DEFAULT", "download_storage"))


def sha256sum(filename):
    h = hashlib.sha256()
    b = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()


def md5sum(filename):
    h = hashlib.md5()
    b = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()


def hash_string(string):
    return hashlib.sha256(string.encode()).hexdigest()
