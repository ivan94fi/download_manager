import sys
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from PyQt5.QtWidgets import QApplication

from downloads_model import DownloadsModel
from controller import Controller
from ui_windows import (DownloadManagerMainWindow,
                        NewDownloadDialog,
                        AboutDialog,
                        PreferencesWindow)
from settings import app_config

if __name__ == '__main__':

    app = QApplication(sys.argv)

    # Instatiate components and perform dependency injection.
    controller = Controller()
    new_download_dialog = NewDownloadDialog()
    about_dialog = AboutDialog()
    preferences_window = PreferencesWindow()
    window = DownloadManagerMainWindow(
        new_download_dialog, about_dialog, preferences_window)

    controller.set_window(window)

    max_workers = app_config.getint("app", "max_workers")
    if app_config.getboolean("app", "use_processes"):
        pool = ProcessPoolExecutor(max_workers=max_workers)
    else:
        pool = ThreadPoolExecutor(max_workers=max_workers)

    model = DownloadsModel(pool=pool)

    controller.set_model(model)
    controller.set_window_model()
    controller.wire_model_and_view()

    # On startup, read the stored configurations and re-start previous
    # pending downloads
    controller.add_previous_downloads()

    window.setWindowTitle('Download Manager')
    window.show()

    sys.exit(app.exec_())
