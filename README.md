# Progettino Human Computer Interaction
## Barebone download manager
### Introduzione
Questo progetto rappresenta l'elaborato individuale per il corso di Human Computer Interaction. Consiste in un download manager multithreaded che permette lo scaricamento di più file in parallelo.

### Pre-requisiti
Il progetto si appoggia a `PyQt5` per la gestione dell'interfaccia grafica e alla libreria `requests` per quanto riguarda lo scaricamento dei file. Inoltre, `humanize` è usata per il display in formato umano di grandezze di file e unità di tempo. È pertanto necessario installare queste ultime per poter eseguire il progetto.
```
$ pip install PyQt5 requests humanize
```

### Installazione
```
$ git clone git@bitbucket.org:ivan94fi/download_manager.git
```

### Utilizzo
```
$ cd download_manager
$ python main.py
```

### Funzionalità
* È possibile portare avanti più download simultaneamente.
* I download sono interrompibili e riprendibili.
* Viene mantenuta una sessione utente attraverso i vari accessi all'applicativo, pertanto è possibile riprendere download interrotti nelle sessioni precedenti e vedere uno storico dei download.
* Ogni download è svolto in un thread a sé stante, per aumentare la responsività dell'applicazione (il GIL di python è un problema trascurabile in quanto l'applicazione è IO-bound piuttosto che CPU-bound).
#### Configurazioni di default
* Numero workers: `10`.
* Cartella download: `<cartella installazione>/downloads`.
* Cartella configurazioni: `<home utente>/.download_manager`.
* Tipo di worker: `thread` (non configurabile).
### Note
* L'uso del multiprocessing è attualmente disabilitato, sebbene fosse inizialmente previsto. Pertanto i worker possono essere solo di tipo thread.
* Le preferenze all'interno dell'applicazione permettono di scegliere la cartella di download ed il numero massimo di workers presenti nel pool.
* Se il server contattato per un download parziale non risponde con un header "Content-Range", l'applicazione fallisce e non è in grado di riprendere il download. Il fallimento non è gestito.
