import datetime
from humanize import naturalsize, naturaldelta
from PyQt5.QtCore import Qt, QModelIndex, QAbstractTableModel, QTimer

from download_config import ACTIVE, PAUSED, STOPPED, COMPLETED


class DownloadsModel(QAbstractTableModel):
    """ Downloads model. This class follows Qt's own Model/View paradigm.

        This class holds a list of downloads to be displayed in a table view.
    """

    def __init__(self, downloads=[], header=None, pool=None):
        super(DownloadsModel, self).__init__()
        self.downloads = downloads
        if header is None:
            self.header = ["Name", "Bytes", "Progress", "Status",
                           "Speed", "Time left", "Added on",
                           "Completed on", "Total time"]
            self._keys = ["name", "bytes_written", "progress", "status",
                          "speed", "time_left", "added",
                          "completed", "total_time"]

        # Thread/Process pool
        self.pool = pool

        # Timer to update view every second.
        self.timer = QTimer(self)

    def rowCount(self, index=QModelIndex()):
        """ Returns the number of downloads to be displayed. """
        return len(self.downloads)

    def columnCount(self, index=QModelIndex()):
        """ Returns the number of columns to be displayed. """
        return len(self.header)

    def data(self, index, role=Qt.DisplayRole):
        """ Returns data specified by the index and role. """
        if not self._is_correct_index(index):
            return None

        if role == Qt.DisplayRole:
            # Take the raw value from the model
            raw_value = self.downloads[index.row()].get_config(
                self._keys[index.column()])

            # Process the raw value according to its semantics.
            # Name
            if index.column() == 0:
                return raw_value

            # Bytes
            elif index.column() == 1:
                filesize = self.downloads[index.row()].get_config("filesize")
                if filesize < 0:
                    return "--"
                if filesize == raw_value:
                    return "{}/{}".format(naturalsize(raw_value).split()[0],
                                          naturalsize(filesize))
                return "{}/{}".format(naturalsize(raw_value),
                                      naturalsize(filesize))

            # Progress
            elif index.column() == 2:
                return raw_value

            # Status
            elif index.column() == 3:
                if raw_value == ACTIVE:
                    text = "Active"
                elif raw_value == PAUSED:
                    text = "Paused"
                elif raw_value == STOPPED:
                    text = "Stopped"
                elif raw_value == COMPLETED:
                    text = "Completed"
                return text

            # Speed
            elif index.column() == 4:
                if raw_value != "--":
                    value = "{}/s".format(naturalsize(raw_value))
                else:
                    value = raw_value
                return value

            # Time left
            elif index.column() == 5:
                if raw_value == "--":
                    return raw_value
                try:
                    value = naturaldelta(datetime.timedelta(seconds=raw_value))
                    return value
                except OverflowError:
                    return "--"

            # Added on
            elif index.column() == 6:
                return raw_value
            # Completed on
            elif index.column() == 7:
                return raw_value
            # Total time
            elif index.column() == 8:
                return raw_value

        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        """ Returns the column headers to be displayed (name, speed,..)
        """
        if role != Qt.DisplayRole:
            return None

        if orientation == Qt.Horizontal:
            return self.header[section]

        return None

    def get_model(self):
        return self.downloads

    def set_model(self, data):
        self.downloads = data
        self.layoutChanged.emit()

    def is_in_model(self, url):
        """ Check if a download with this url is already in the model list. """
        for download in self.downloads:
            if download.get_config('url') == url:
                return True
        return False

    def _signal_row_changed(self, row):
        """ Emits a dataChanged with the index corresponding to a certain row.
        """
        top_left = self.createIndex(row, 0)
        bottom_right = self.createIndex(row, self.columnCount())
        self.dataChanged.emit(top_left, bottom_right)

    def add_download_to_model(self, new_download):
        """ Adds a new download to the list of currently available downloads.

            Returns the index of the newly added download.
        """
        self.downloads.append(new_download)
        row = self.rowCount() - 1
        self.layoutChanged.emit()

        # Connect to the timer: update row every second.
        self._connect_to_timer(lambda: self._signal_row_changed(row))
        if not self.timer.isActive():
            self._start_timer()
        return row

    def _is_correct_index(self, index):
        """ Checks whether the index is in the correct row/column range. """
        return (index.isValid()
                and 0 <= index.row() < self.rowCount()
                and 0 <= index.column() < self.columnCount())

    def _start_timer(self):
        self.timer.start(1000)

    def _stop_timer(self):
        self.timer.stop()

    def _connect_to_timer(self, slot):
        self.timer.timeout.connect(slot)
