import os
from configparser import ConfigParser
import pickle

# These are the defult settings. They will be written on disk at the first
# app execution.
########################################################
SETTINGS_DIR = os.path.join(os.path.expanduser('~'), '.download_manager')

CONFIG_FILE_PATH = os.path.join(SETTINGS_DIR, 'config.ini')

DOWNLOAD_CONFIG_STORAGE_PATH = os.path.join(
    SETTINGS_DIR, 'download_storage.pkl')

USE_PROCESSES = False

MAX_WORKERS = 10
########################################################

if not os.path.isdir(SETTINGS_DIR):
    os.mkdir(SETTINGS_DIR)

if not os.path.exists(CONFIG_FILE_PATH):
    os.mknod(CONFIG_FILE_PATH)

if not os.path.exists(DOWNLOAD_CONFIG_STORAGE_PATH):
    os.mknod(DOWNLOAD_CONFIG_STORAGE_PATH)

if os.path.getsize(DOWNLOAD_CONFIG_STORAGE_PATH) == 0:
    with open(DOWNLOAD_CONFIG_STORAGE_PATH, 'wb') as config_file:
        pickle.dump({}, config_file)

if not os.path.exists('./downloads'):
    os.mkdir('./downloads')

# Initialize a config parser to read the .ini file. This variable will be
# shared by every module (the original idea was to use a singleton, but
# singletons are discouraged in python -- at least this is the general idea
# on StackOverflow).
app_config = ConfigParser()
app_config.read(CONFIG_FILE_PATH)


def write_app_config():
    with open(CONFIG_FILE_PATH, 'w') as configfile:
        app_config.write(configfile)


# Sets the default settings first. Will be overwritten by "app" settings-
if not app_config.has_section("DEFAULT"):
    app_config.set("DEFAULT", "settings_dir", SETTINGS_DIR)
    app_config.set("DEFAULT", "config_file_path", CONFIG_FILE_PATH)
    CONFIG_FILE_PATH = os.path.join(SETTINGS_DIR, 'config.ini')
    app_config.set("DEFAULT", "download_storage",
                   DOWNLOAD_CONFIG_STORAGE_PATH)
    app_config.set("DEFAULT", "download_dir", './downloads')
    app_config.set("DEFAULT", "use_processes", str(USE_PROCESSES))
    app_config.set("DEFAULT", "max_workers", str(MAX_WORKERS))

    if not app_config.has_section("app"):
        app_config.add_section("app")

    write_app_config()
