from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QStyle, QApplication, QProgressBar


class ProgressBarDelegate(QtWidgets.QItemDelegate):
    """ Delegate class to handle the representation of the progress.
        Transforms an int value in a progressbar widget.
    """

    def __init__(self, parent=None, *args, **kwargs):
        super(ProgressBarDelegate, self).__init__(parent=None, *args, **kwargs)
        self.parent = parent

    def paint(self, painter, option, index):
        if index.column() == 2:
            raw_value = index.data(Qt.DisplayRole)
            renderer = QProgressBar()

            style = "QProgressBar { border: none; border-radius: 0px; }"
            style += "QProgressBar::chunk { background-color: #5ddb4f;}"

            renderer.resize(option.rect.size())
            renderer.setMinimum(0)
            renderer.setMaximum(100)
            renderer.setValue(raw_value)
            renderer.setAlignment(Qt.AlignCenter)

            renderer.setStyleSheet(style)
            painter.save()
            painter.translate(option.rect.topLeft())
            renderer.render(painter)
            painter.restore()

            QApplication.style().drawControl(
                QStyle.CE_ProgressBar, option, painter)
