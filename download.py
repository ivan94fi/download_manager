import uuid
import traceback
from PyQt5.QtCore import QObject, pyqtSignal

import requests

import download_config
from utils import config_manager
from settings import app_config


def threaded(fn):
    """ Creates a wrapper to execute the wrapped function as a thread in
        the local thread pool.
    """

    def wrapper(self, *args, **kwargs):
        return self.pool.submit(fn, self, *args, **kwargs)
    return wrapper


class Download(QObject):
    """ Class to encapsulate all network functionality for a download job.
    """

    # Signals
    download_started = pyqtSignal(uuid.UUID)
    download_completed = pyqtSignal(uuid.UUID)
    download_aborted = pyqtSignal(uuid.UUID)

    def __init__(self, url, pool,
                 chunk_size=4096,
                 download_directory=app_config["app"]["download_dir"],
                 init_from_file=False):
        super(Download, self).__init__()

        # Initialize the configuration options for this download.
        self.config = download_config.DownloadConfig(
            url, download_directory,
            init_from_file=init_from_file, chunk_size=chunk_size)

        # Set the workers pool
        self.pool = pool

        self.block_now = False

    @threaded
    def start(self):
        """ Start the download in a streamed request working in a new thread:
            reads the contents one chunk at a time from the source.
        """
        self.download_started.emit(self.config['id'])
        print("starting")

        # If the file was already partially downloaded, send a request with a
        # range header to get only what is missing.
        headers = {}
        bytes_written = self.get_config("bytes_written")
        partial = True if bytes_written > 0 else False
        if partial:
            headers = {'Range': 'bytes={}-'.format(bytes_written)}

        try:
            with requests.get(self.config['url'],
                              headers=headers, stream=True) as response:
                if partial:
                    # If the response is expected to be partial,
                    # 'Content-Range' should be in the headers.
                    try:
                        response.headers['Content-Range']
                    except KeyError:
                        raise

                # Check for successful connection.
                response.raise_for_status()

                # Start writing the response one chunk at a time.
                with open(self.config['filepath'], 'ab') as f:
                    for chunk_num, chunk in enumerate(
                            response.iter_content(
                            chunk_size=self.get_config('chunk_size'))):

                        # This is kinda hacky, but this function runs in a
                        # separate thread and i have not found a cleaner way.
                        if self.must_stop_download():
                            config_manager.store_config(self.config)
                            break

                        if chunk:
                            f.write(chunk)
                            self.config.update(chunk_num, len(chunk))

            if not self.must_stop_download():
                self.config.completed()
                self.download_completed.emit(self.config['id'])

        except Exception as e:
            # TODO: Handle the exceptions thrown and maybe remove download..
            print("Exception:", repr(e))
            print(traceback.format_exc())
            self.download_aborted.emit(self.config['id'])

    def get_config(self, key):
        return self.config[key]

    def must_stop_download(self):
        """ Returns true if the download procedure shoulf be stopped.
        """
        return (self.block_now
                or self.get_config('status') == download_config.PAUSED
                or self.get_config('status') == download_config.STOPPED)
