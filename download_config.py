import datetime
import os
import time
import uuid
from PyQt5.QtCore import QObject, pyqtSignal

import requests

from utils import config_manager


ACTIVE = 1
PAUSED = 2
STOPPED = 3
COMPLETED = 4


class ObservableDict(QObject):
    """ A class implementing observable dict values. """

    # The signal used to broadcast changes.
    # !! THIS IS NOT USED !! (updates too frequently)
    value_changed = pyqtSignal(str, int)

    def __init__(self):
        super().__init__()
        self._dict = {}

    def __len__(self):
        return len(self._dict)

    def __iter__(self):
        return iter(self._dict)

    def __getitem__(self, key):
        return self._dict[key]

    # Signal when we change a key.value pair.
    def __setitem__(self, key, value):
        self._dict[key] = value
        self.value_changed.emit(key, value)

    # Register slot to changes in observed values.
    def observe(self, slot):
        self.value_changed.connect(slot)


class DownloadConfig(ObservableDict):
    """ An observable dictionary keeping all metadata for a download job. """

    def __init__(self, url, download_directory,
                 name=None, init_from_file=False, chunk_size=4096):
        super(DownloadConfig, self).__init__()

        # Initialize the configuration from the configuration storage.
        # This is to have persistency of past download jobs.
        if init_from_file:
            self._dict = config_manager.read_config(url)

            # Realign the number of written bytes with the correct one.
            if self._dict['bytes_written'] != os.path.getsize(self._dict['filepath']):
                self._dict['bytes_written'] = os.path.getsize(
                    self._dict['filepath'])

        # Initialize the config for a new download, not present in past
        # downloads storage.
        else:
            if not url.startswith('http://') and not url.startswith('https://'):
                url = 'http://' + url
            self._dict['url'] = url
            self._dict['id'] = uuid.uuid5(uuid.NAMESPACE_DNS, url)
            if not name:
                self._dict['name'] = self._dict['url'].split(
                    '/')[-1].split('?')[0].strip()
                if self._dict['name'] == "":
                    time_string = time.strftime(
                        '%d-%b-%y_%H-%M', time.localtime())
                    self._dict['name'] = "download_" + time_string
                    print("Assigning a default name to the downloaded file:")
                    print(self._dict['name'], '\n')

            self._dict['chunk_size'] = chunk_size
            self._dict['download_directory'] = os.path.abspath(
                download_directory)
            self._dict['filepath'] = os.path.join(
                self._dict['download_directory'],
                self._dict['name'])
            self._dict['status'] = ACTIVE
            self._dict['progress'] = 0
            self._dict['speed'] = 0
            self._dict['added'] = time.strftime(
                '%a %d %b %Y, %H:%M:%S', time.localtime())

            r = requests.get(self._dict['url'], stream=True)
            try:
                self._dict['filesize'] = int(r.headers['content-length'])
            except KeyError:
                print("Unable to acquire file size.")
                self._dict['filesize'] = -1
                import sys
                sys.exit(1)

            self._dict['bytes_written'] = 0
            self._dict['start_time'] = 0

            # Utilized to compute speed and ETA.
            self._dict['step'] = 0
            self._dict['size_at_step'] = 0

            self._dict['time_left'] = "--"
            self._dict['completed'] = "--"
            self._dict['total_time'] = "--"

            # Persist the newly created cofiguration
            config_manager.store_config(self)

    def update(self, chunk_number, chunk_len):
        """ Called everytime that a chunk is written on disk.
            Updates the config accordingly.
        """
        self._dict['bytes_written'] += chunk_len
        if chunk_number == 0:
            self._dict['start_time'] = time.time()
            self._dict['step'] = self._dict['start_time']
            self._dict['size_at_step'] = self._dict['bytes_written']
            return

        self._dict['progress'] = int((self._dict['bytes_written']
                                      / self._dict['filesize']) * 100)

        new_step = time.time()
        # Ensure that at least a second is passed since the last step
        # before calculating the current speed: this is done to avoid
        # numerical errors due to delta_t being too small.
        if new_step - self._dict['step'] >= 1:
            delta_t = new_step - self._dict['step']
            delta_size = (self._dict['bytes_written']
                          - self._dict['size_at_step'])
            speed = delta_size / delta_t
            self._dict['speed'] = speed

            self._dict['time_left'] = (
                (self._dict['filesize'] - self._dict['bytes_written'])
                / self._dict['speed'])

            # Record at what time the last speed computation was done.
            self._dict['step'] = new_step
            self._dict['size_at_step'] = self._dict['bytes_written']

    def completed(self):
        """ Called when the download was succesfully completed.
        """
        self._dict['completed'] = time.strftime(
            '%a %d %b %Y, %H:%M:%S', time.localtime())
        self._dict['speed'] = '--'
        self._dict['status'] = COMPLETED

        t_added = datetime.datetime.strptime(
            self._dict['added'], '%a %d %b %Y, %H:%M:%S')
        t_completed = datetime.datetime.strptime(
            self._dict['completed'], '%a %d %b %Y, %H:%M:%S')

        self._dict['total_time'] = str(t_completed - t_added)
        self._dict['time_left'] = "--"
        self._dict['progress'] = 100
        self._dict['bytes_written'] = self._dict['filesize']
        config_manager.store_config(self)
