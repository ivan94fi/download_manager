# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'about_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AboutDialog(object):
    def setupUi(self, AboutDialog):
        AboutDialog.setObjectName("AboutDialog")
        AboutDialog.resize(400, 160)
        self.about_dialog_label = QtWidgets.QLabel(AboutDialog)
        self.about_dialog_label.setGeometry(QtCore.QRect(40, 10, 331, 121))
        self.about_dialog_label.setLineWidth(1)
        self.about_dialog_label.setTextFormat(QtCore.Qt.AutoText)
        self.about_dialog_label.setWordWrap(True)
        self.about_dialog_label.setObjectName("about_dialog_label")
        self.ok_button = QtWidgets.QPushButton(AboutDialog)
        self.ok_button.setGeometry(QtCore.QRect(300, 120, 80, 23))
        self.ok_button.setObjectName("ok_button")

        self.retranslateUi(AboutDialog)
        self.ok_button.clicked.connect(AboutDialog.close)
        QtCore.QMetaObject.connectSlotsByName(AboutDialog)

    def retranslateUi(self, AboutDialog):
        _translate = QtCore.QCoreApplication.translate
        AboutDialog.setWindowTitle(_translate("AboutDialog", "Dialog"))
        self.about_dialog_label.setText(_translate("AboutDialog", "Barebone Download Manager built as mini-project for the \"Human Computer Interaction\" exam.\n"
"\n"
"Ivan Prosperi - ivan.prosperi@stud.unifi.it\n"
"\n"
"2019"))
        self.ok_button.setText(_translate("AboutDialog", "Ok"))

