import os
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QDialog, QWidget, QFileDialog
from PyQt5.QtWidgets import QMessageBox, QTableView

from ui_main_window import Ui_MainWindow
from ui_new_download_dialog import Ui_NewDownloadDialog
from ui_about_dialog import Ui_AboutDialog
from ui_preferences import Ui_Preferences
from utils import config_manager
from delegates import ProgressBarDelegate
from settings import app_config, write_app_config
import download_config

""" This file contains the implementations of widgets generated with Designer.
"""


class PreferencesWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Preferences()
        self.ui.setupUi(self)
        download_dir = os.path.abspath(app_config["app"]["download_dir"])
        self.ui.download_dir_label.setText(download_dir)
        self.dirty = False
        use_processes = app_config["app"]["use_processes"]
        if use_processes == "True":
            print("toggle process")
            self.ui.process_radio_button.toggle()
        else:
            self.ui.thread_radio_button.toggle()
        max_workers = int(app_config["app"]["max_workers"])
        self.ui.max_workers_spin_box.setValue(max_workers)
        self.ui.edit_dir_button.clicked.connect(self.choose_download_dir)

        self.ui.process_radio_button.toggled.connect(
            lambda: self.choose_worker_type("process"))
        self.ui.thread_radio_button.toggled.connect(
            lambda: self.choose_worker_type("thread"))

        self.ui.max_workers_spin_box.valueChanged.connect(
            lambda n: self.choose_max_workers(n))

        self.ui.ok_button.clicked.connect(lambda: self.check_dirty())

    def choose_download_dir(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.Directory)
        if file_dialog.exec_():
            filenames = file_dialog.selectedFiles()
            if filenames:
                new_download_dir = filenames[0]
                if new_download_dir != self.ui.download_dir_label.text():
                    self.ui.download_dir_label.setText(new_download_dir)
                    app_config["app"]["download_dir"] = new_download_dir
                    self.dirty = True
                    write_app_config()

    def choose_worker_type(self, worker_type):
        if worker_type == "process":
            app_config["app"]["use_processes"] = "True"
        else:
            app_config["app"]["use_processes"] = "False"

        self.dirty = True
        write_app_config()

    def choose_max_workers(self, max_workers):
        app_config["app"]["max_workers"] = str(max_workers)

        self.dirty = True
        write_app_config()

    def check_dirty(self):
        if self.dirty:
            message_box = QMessageBox()
            message_box.setText(
                "The modifications will be effective at next startup.")
            message_box.setIcon(1)
            message_box.exec()


class NewDownloadDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_NewDownloadDialog()
        self.ui.setupUi(self)
        self.setFixedSize(self.geometry().width(), self.geometry().height())
        self.ui.line_edit.setFocus()

        # Some possible default downloads used in development.
        # default_url = 'https://mirror.selfnet.de/lineageos/full/cheeseburger/20190407/lineage-16.0-20190407-nightly-cheeseburger-signed.zip'
        # default_url = 'http://httpbin.org/bytes/4096'
        # default_url = 'https://bitbucket.org/ivan94fi/hci_project_report/raw/5f2b78ff177cdcb8cdc6147347f827bc7e7e83c5/.gitignore'
        # default_url = 'https://franco-lnx.net/OnePlus5/9-custom/anyKernel/fk-r44-anykernel2.zip'
        default_url = ''
        self.ui.line_edit.setText(default_url)


class AboutDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)
        self.setFixedSize(self.geometry().width(), self.geometry().height())


class DownloadManagerMainWindow(QMainWindow):
    def __init__(self, new_download_dialog, about_dialog, preferences_window):

        super().__init__()
        self._new_download_dialog = new_download_dialog
        self._about_dialog = about_dialog
        self._preferences_window = preferences_window

        # Set up the user interface from Designer.
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.table_view.horizontalHeader().setStretchLastSection(True)
        self.ui.table_view.setStyleSheet(
            "QTableView{ selection-background-color: #7eceda;  }")
        self.ui.table_view.setSelectionBehavior(QTableView.SelectRows)

        # Set up the delegate for the progress bar: it mediates between views
        # and models to customize the final rendering of data.
        self.progress_bar_delegate = ProgressBarDelegate(self.ui.table_view)
        self.ui.table_view.setItemDelegateForColumn(
            2, self.progress_bar_delegate)

        # Wire actions to injected dialogs proper methods
        self.ui.action_new_download.triggered.connect(
            self._new_download_dialog.exec_)
        self.ui.action_about.triggered.connect(
            self._about_dialog.exec_)
        self.ui.action_preferences.triggered.connect(
            self._preferences_window.show)

        # Connect the quit button to the exit signal
        self.ui.action_quit.triggered.connect(self.closeEvent)

    def closeEvent(self, event):
        """ Intercept close event to stop downloads and save their state.
        """
        print("Saving pending downloads...")
        config_manager._timer.cancel()
        config_manager._dirty = True
        config_manager._save_config_file()
        self.ui.table_view.model().pool.shutdown(wait=False)
        for download in self.ui.table_view.model().downloads:
            if download.get_config('status') == download_config.ACTIVE:
                download.block_now = True

        QApplication.exit()
