import os
from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtWidgets import QMessageBox, QAction, QMenu

import download_config
from download import Download
from utils import config_manager


class Controller(object):
    """ Class to manage model and view comunication.
    """

    def __init__(self, window=None, model=None):
        super(Controller, self).__init__()
        self._window = window
        self._model = model

        self.selected_index = None

    def set_window(self, window):
        self._window = window

    def get_window(self):
        return self._window

    def get_model(self):
        return self._model

    def set_model(self, model):
        self._model = model

    def set_window_model(self):
        if self._model is None:
            raise ValueError(("The model must be explicitly set on the ",
                              "controller before setting it in the view."))

        self._window.ui.table_view.setModel(self._model)

    def wire_model_and_view(self):
        """ This method was originally meant to connect model signals to
            views updates but these connections were eventually moved to
            the view itself. Only some addtional initializations remain here.
        """
        if self._model is None or self._window is None:
            raise ValueError(("Plese set model and view before ",
                              "trying to wire connections."))

        cols = self._model.columnCount()
        column_widths = [200, 130, 130, 100, 100, 100, 130, 130, 130]
        for i in range(cols):
            self._window.ui.table_view.setColumnWidth(i, column_widths[i])

        self.setup_context_menu()
        self._window._new_download_dialog.accepted.connect(
            lambda: self.add_new_download(
                self._window._new_download_dialog.ui.line_edit.text()))

    def setup_context_menu(self):
        """ Handles the context menu for the table view.
        """
        self.table_menu = QMenu(self._window.ui.table_view)
        self.action_menu_pause = QAction("Pause", self._window)
        self.action_menu_start = QAction("Start", self._window)
        self.action_menu_remove = QAction("Remove", self._window)

        self.table_menu.addAction(self.action_menu_pause)
        self.table_menu.addAction(self.action_menu_start)
        self.table_menu.addAction(self.action_menu_remove)

        self._window.ui.table_view.setContextMenuPolicy(Qt.CustomContextMenu)
        self._window.ui.table_view.customContextMenuRequested.connect(
            lambda pos: self.open_table_context_menu(pos))

        self.action_menu_start.triggered.connect(
            lambda: self.start_download())
        self.action_menu_pause.triggered.connect(
            lambda: self.pause_download())
        self.action_menu_remove.triggered.connect(
            lambda: self.remove_download())

    def open_table_context_menu(self, pos=None):
        index = self._window.ui.table_view.indexAt(pos)
        if index.isValid():
            self.selected_index = index
            parent = self._window.sender()
            parent_pos = parent.mapToGlobal(QPoint(5, 20))
            pos = parent_pos + pos
            self.table_menu.move(pos)
            self.table_menu.show()

    def start_download(self):
        row = self.selected_index.row()
        if self._model.downloads[row].config[
                'status'] == download_config.ACTIVE:
            return
        else:
            self._model.downloads[row].config[
                'status'] = download_config.ACTIVE
            self._model.downloads[row].block_now = False
            self._model.downloads[row].start()

    def pause_download(self):
        row = self.selected_index.row()
        self._model.downloads[row].config['status'] = download_config.PAUSED

    def remove_download(self):
        row = self.selected_index.row()
        download_to_remove = self._model.downloads[row]

        config_manager.remove_config(download_to_remove.get_config('url'))
        self._model.downloads.remove(download_to_remove)

    def add_previous_downloads(self):
        """ Reads the download storage and sets up the previous session's
            downloads.
        """
        config_dict = config_manager.get_config_dict()
        for k, v in config_dict.items():
            if os.path.isfile(v['filepath']):
                self.add_new_download(v['url'], from_file=True)
            else:
                config_manager.remove_config(v['url'])

    def add_new_download(self, url, from_file=False):
        """ Creates a new download with the given url and adds it to the
            model. Performs all the necessary wiring.

        """
        new_download = Download(url, self._model.pool,
                                init_from_file=from_file)

        # Check if a download with the same url of the downlaod being added is
        # already downloading.
        if self._model.is_in_model(url):
            if new_download.get_config('status') == download_config.ACTIVE:
                message_box = QMessageBox()
                message_box.setText("The download is already active.")
                message_box.setIcon(2)
                message_box.exec()
                return

        # If we are adding a brand new download, check before if the file
        # already exists.
        if not from_file:
            if os.path.isfile(new_download.get_config('filepath')):
                answer = QMessageBox.question(
                    self._window, 'Warning', "File already exists. Overwrite?",
                    QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if answer == QMessageBox.Yes:
                    os.remove(new_download.get_config("filepath"))
                if answer == QMessageBox.No:
                    return

        # Add the download to the download list in the model: it will be shown
        # in the main window
        self._model.add_download_to_model(new_download)
        new_download.download_started.connect(
            lambda uuid: print("download {} started".format(uuid)))
        new_download.download_completed.connect(
            lambda uuid: print("download {} completed".format(uuid)))

        # Finally, if the download is marked with active status, start it.
        if new_download.get_config('status') == download_config.ACTIVE:
            new_download.start()
