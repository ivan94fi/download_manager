# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new_download_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_NewDownloadDialog(object):
    def setupUi(self, NewDownloadDialog):
        NewDownloadDialog.setObjectName("NewDownloadDialog")
        NewDownloadDialog.resize(400, 145)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(NewDownloadDialog.sizePolicy().hasHeightForWidth())
        NewDownloadDialog.setSizePolicy(sizePolicy)
        NewDownloadDialog.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.button_box = QtWidgets.QDialogButtonBox(NewDownloadDialog)
        self.button_box.setGeometry(QtCore.QRect(37, 100, 341, 32))
        self.button_box.setOrientation(QtCore.Qt.Horizontal)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.button_box.setObjectName("button_box")
        self.label = QtWidgets.QLabel(NewDownloadDialog)
        self.label.setGeometry(QtCore.QRect(20, 20, 251, 20))
        self.label.setObjectName("label")
        self.line_edit = QtWidgets.QLineEdit(NewDownloadDialog)
        self.line_edit.setGeometry(QtCore.QRect(20, 50, 361, 23))
        self.line_edit.setInputMethodHints(QtCore.Qt.ImhNoAutoUppercase|QtCore.Qt.ImhUrlCharactersOnly)
        self.line_edit.setText("")
        self.line_edit.setDragEnabled(True)
        self.line_edit.setClearButtonEnabled(False)
        self.line_edit.setObjectName("line_edit")

        self.retranslateUi(NewDownloadDialog)
        self.button_box.accepted.connect(NewDownloadDialog.accept)
        self.button_box.rejected.connect(NewDownloadDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(NewDownloadDialog)

    def retranslateUi(self, NewDownloadDialog):
        _translate = QtCore.QCoreApplication.translate
        NewDownloadDialog.setWindowTitle(_translate("NewDownloadDialog", "New Download"))
        self.label.setText(_translate("NewDownloadDialog", "Insert the new download url:"))
        self.line_edit.setPlaceholderText(_translate("NewDownloadDialog", "http:// ..."))

